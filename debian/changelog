winexe (1.1~20140107-0kali14) kali-dev; urgency=medium

  * Add smoke test

 -- Arnaud Rebillout <arnaudr@kali.org>  Tue, 26 Apr 2022 07:54:15 +0700

winexe (1.1~20140107-0kali13) kali-experimental; urgency=medium

  [ Ben Wilson ]
  * Update email address

  [ Arnaud Rebillout ]
  * Build-Depends += bison, flex
  * Build-Depends += python3-markdown
  * Build-Depends += python3-dnspython
  * Build-Depends += libjson-perl
  * d/rules: Use more variables for readability
  * d/rules: Drop line that has no effect
  * d/rules: Prevent random ordering of heimdal headers
  * d/rules: Drop systemd fix, not needed anymore
  * d/rules: Cosmetics, use spaces in assignments
  * Update 'dropped-headers' patch for samba libraries that were renamed
  * Add patch to update cli_credentials_set_kerberos_state()

 -- Arnaud Rebillout <arnaudr@kali.org>  Mon, 25 Apr 2022 17:43:05 +0700

winexe (1.1~20140107-0kali12) kali-dev; urgency=medium

  [ Kali Janitor ]
  * Set upstream metadata fields: Repository.
  * Update renamed lintian tag names in lintian overrides.

  [ Sophie Brun ]
  * Add missing build-dep libdbus-1-dev
  * Update debian/watch to latest version 4
  * Bump Standards-Version to 4.5.0

 -- Sophie Brun <sophie@offensive-security.com>  Mon, 07 Dec 2020 15:22:28 +0100

winexe (1.1~20140107-0kali11) kali-dev; urgency=medium

  * Add missing build-dep: libparse-yapp-perl

 -- Sophie Brun <sophie@offensive-security.com>  Fri, 17 Jul 2020 10:49:08 +0200

winexe (1.1~20140107-0kali10) kali-dev; urgency=medium

  [ Raphaël Hertzog ]
  * Work around a bug breaking samba's build
  * Add some lintian overrides

 -- Sophie Brun <sophie@offensive-security.com>  Fri, 17 Jul 2020 10:49:04 +0200

winexe (1.1~20140107-0kali9) kali-experimental; urgency=medium

  * Use "waf -j 1" for configure step

 -- Raphaël Hertzog <raphael@offensive-security.com>  Mon, 21 Oct 2019 17:17:02 +0200

winexe (1.1~20140107-0kali8) kali-experimental; urgency=medium

  * Switch to Python 3
  * Update waf for Python3
  * fix the build with new samba version
  * Add debian/source/lintian-overrides
  * Add a patch to fix spelling error
  * Bump Standards-Version to 4.4.1
  * Use debhelper-compat 12

 -- Sophie Brun <sophie@offensive-security.com>  Mon, 07 Oct 2019 15:51:36 +0200

winexe (1.1~20140107-0kali7) kali-dev; urgency=medium

  * Add missing build-deps for the new version of samba: liblmdb-dev,
    libjansson-dev, libgpgme-dev, libarchive-dev

 -- Sophie Brun <sophie@offensive-security.com>  Thu, 08 Nov 2018 14:16:57 +0100

winexe (1.1~20140107-0kali6) kali-dev; urgency=medium

  * Add missing build-dependency: libpam0g-dev

 -- Sophie Brun <sophie@offensive-security.com>  Thu, 02 Nov 2017 11:23:02 +0100

winexe (1.1~20140107-0kali5) kali-dev; urgency=medium

  * No change rebuild.

 -- Raphaël Hertzog <buxy@kali.org>  Wed, 05 Jul 2017 15:36:55 +0200

winexe (1.1~20140107-0kali4) kali-dev; urgency=medium

  * Download samba at runtime and add another patch to work around
    headers dropped from the public headers available in Debian packages.

 -- Raphaël Hertzog <buxy@kali.org>  Wed, 18 May 2016 12:58:26 +0200

winexe (1.1~20140107-0kali3) kali-dev; urgency=medium

  * Add a patch to build against samba-libs version 4.3

 -- Sophie Brun <sophie@offensive-security.com>  Tue, 01 Mar 2016 11:08:01 +0100

winexe (1.1~20140107-0kali2) kali-dev; urgency=medium

   * No change rebuild, just to increase the version number.

 -- Raphaël Hertzog <buxy@kali.org>  Mon, 31 Aug 2015 12:00:27 +0200

winexe (1.1~20140107-0kali1) kali-dev; urgency=medium

  * New upstream release
  * Drop build-dependencies that have been merged into samba-dev/samba-libs.
  * Drop patch that has been merged upstream.

 -- Raphaël Hertzog <buxy@kali.org>  Thu, 07 Aug 2014 13:51:15 +0000

winexe (1.1~20130620-0kali5) kali; urgency=low

  * Do no ship /usr/bin/bin2c (FS#715).

 -- Raphaël Hertzog <buxy@kali.org>  Thu, 28 Nov 2013 13:49:36 +0100

winexe (1.1~20130620-0kali4) kali-dev; urgency=low

  * Replace python-samba Build-Depends by samba-common-bin which really
    contains the library required by libsamba-credentials0 in the
    current version of the samba 4.0 packages.
  * Enable verbose build logs and add -Wall to CFLAGS.
  * Add use-pkgconfig patch to use the proper CFLAGS when building
    winexe against Samba's shared libraries.

 -- Raphaël Hertzog <buxy@kali.org>  Fri, 28 Jun 2013 22:19:33 +0200

winexe (1.1~20130620-0kali3) kali-dev; urgency=low

  * Disable entirely hardening as other options also cause problems.

 -- Raphaël Hertzog <buxy@kali.org>  Thu, 20 Jun 2013 23:03:55 +0200

winexe (1.1~20130620-0kali2) kali-dev; urgency=low

  * Disable "relro" hardening option as /usr/bin/x86_64-w64-mingw32-ld doesn't
    support the "-Wl,-z,relro" option.

 -- Raphaël Hertzog <buxy@kali.org>  Thu, 20 Jun 2013 22:09:09 +0200

winexe (1.1~20130620-0kali1) kali-dev; urgency=low

  * Initial release.
  * Add Build-Depends on python-samba to work-around missing dependency on
    libsamba-credentials0 (see #705908).

 -- Raphaël Hertzog <buxy@kali.org>  Thu, 20 Jun 2013 15:55:14 +0200
